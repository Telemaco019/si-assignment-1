package binarySerialization;


import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import serialization.ClassroomProtos;
import textSerialization.Classroom;
import utils.Matematica;

import static utils.Constants.EMAIL_DOMAIN;
import static utils.Constants.NUM_MAX_TELEPHONES_PER_PERSON;

public class BinarySerializationUtils {
    private static final String FILE_NAME = "classroom.bin";
    private static final int NUMBER_OF_STUDENTS = 10;
    private static final String DIRECTOR_NAME = "Director-1";
    private static final String CLASSROOM_NAME = "Classroom-1";
    private static final int NUMBER_OF_EXECUTIONS = 1;


    public static ClassroomProtos.Classroom getClassroom(int numberOfStudents, String directorName, String classroomName) {
        return ClassroomProtos.Classroom.newBuilder()
                .setDirector(getDirector(directorName))
                .addAllStudents(getRandomStudents(numberOfStudents))
                .setName(classroomName)
                .build();
    }

    public static ArrayList<ClassroomProtos.Student> getRandomStudents(int studentNumber) {
        ArrayList<ClassroomProtos.Student> students = new ArrayList<>();
        for (int i = 0; i < studentNumber; i++) {
            String student_name = String.format("student-%d", i);
            String student_mail = String.format("%s%s", student_name, EMAIL_DOMAIN);
            String id = String.format("%d", System.identityHashCode(student_mail + student_name));

            students.add(ClassroomProtos.Student.newBuilder()
                    .setName(student_name)
                    .setEmail(student_mail)
                    .setId(String.format("%d", i))
                    .addAllTelephones(getRandomTelephones())
                    .build());
        }

        return students;
    }

    /**
     * Return a director having the name specified as parameter and one or more random telephones
     *
     * @param name
     * @return
     */
    public static ClassroomProtos.Director getDirector(String name) {
        return ClassroomProtos.Director.newBuilder()
                .setName(name)
                .setEmail(name + EMAIL_DOMAIN)
                .addAllTelephones(getRandomTelephones())
                .setId(String.format("%s", System.identityHashCode(name + System.nanoTime())))
                .build();
    }

    /**
     * Return 1,2 or 3 (randomly) random telephones
     *
     * @return An array list containing 1,2 or 3 (randomly) random telephones
     */
    public static ArrayList<ClassroomProtos.Telephone> getRandomTelephones() {
        String number = String.format("%d", Math.round(Math.random() * (Math.pow(10, 10))));
        int types_number = ClassroomProtos.Telephone.TelephoneType.getDescriptor().getValues().size();
        ArrayList<ClassroomProtos.Telephone> telephones = new ArrayList<>();

        for (int i = 0; i < Matematica.numeroInteroRandom(NUM_MAX_TELEPHONES_PER_PERSON) + 1; i++) {
            ClassroomProtos.Telephone.TelephoneType type = ClassroomProtos.Telephone.TelephoneType.valueOf(Matematica.numeroInteroRandom(types_number));

            ClassroomProtos.Telephone telephone = ClassroomProtos.Telephone.newBuilder()
                    .setNumber(number)
                    .setType(type)
                    .build();

            telephones.add(telephone);
        }

        return telephones;
    }
}
