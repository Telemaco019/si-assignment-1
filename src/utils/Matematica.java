package utils;

import java.util.Random;

public class Matematica {
    private static final String MSG_ERRORE_NUMERO_NEGATIVO = "Errore: impossibile applicare il metodo con numeri negativi";

    /**
     * Restituisce un numero intero casuale compreso tra 0 e l'estremo passato come parametro (escluso)
     *
     * @param estremoEscluso Estremo superiore (escluso)
     * @return Numero casuale compreso tra 0 e l'estremo passato come paramtro (escluso)
     * @throws IllegalArgumentException Se l'estremo è un numero negativo
     */
    public static int numeroInteroRandom(int estremoEscluso) throws IllegalArgumentException {
        if (estremoEscluso < 0)
            throw new IllegalArgumentException(MSG_ERRORE_NUMERO_NEGATIVO);
        Random random = new Random();
        return random.nextInt(estremoEscluso);
    }

    /**
     * Restituisce un numero double casuale compreso nell'intervallo aperto (-Estremo ; +Estremo)
     *
     * @param estremo Estremo positivo dell'intervallo simmetrico
     * @return Un numero compreso nell'intervallo (-Estremo ; +Estremo)
     * @throws IllegalArgumentException Se l'estremo � un numero negativo
     */
    public static double numeroDoubleRandomSimmetrico(double estremo) throws IllegalArgumentException {
        if (estremo < 0)
            throw new IllegalArgumentException(MSG_ERRORE_NUMERO_NEGATIVO);
        Random random = new Random();
        return (random.nextDouble() * estremo * 2 - estremo);

    }

    /**
     * Ritorna il fattoriale del numero passato come parametro
     *
     * @param num Numero di cui si vuole calcolare il fattoriale
     * @return Il fattoriale del numero passato come parametro
     * @throws IllegalArgumentException Se il numero passato come parametro � negativo
     */
    public static int calcolaFattoriale(int num) throws IllegalArgumentException {
        if (num < 0)
            throw new IllegalArgumentException(MSG_ERRORE_NUMERO_NEGATIVO);

        if (num == 0)
            return 1;
        else
            return num * calcolaFattoriale(num - 1);
    }

    public static double getAverage(long array[]) {
        long total = 0L;

        for(long element: array)
            total+= element;

        return (double) total / array.length;
    }

    public static double getStandardDeviation(long array[]) {
        double mean = getAverage(array);
        double sum = 0;
        for(long element: array) {
            sum += Math.pow(element-mean,2);
        }
        return Math.sqrt(sum/array.length);
    }
}
