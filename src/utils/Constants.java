package utils;

public class Constants {
    public static final int NUM_MAX_TELEPHONES_PER_PERSON = 3;
    public static final int NUMBER_OF_STUDENTS = 200;
    public static final String DIRECTOR_NAME = "Director-1";
    public static final String CLASSROOM_NAME = "Classroom-1";
    public static final int NUMBER_OF_EXECUTIONS = 100;
    public static final String EMAIL_DOMAIN = "@gmail.com";

    public static final String XML_FILE_PATH = "src/data/classroom.xml";
    public static final String BINARY_FILE_PATH = "src/data/classroom.bin";
}
