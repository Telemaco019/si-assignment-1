package utils;

import java.util.ArrayList;

/**
 * Contiene metodi di utilita' per operare sulle stringhe
 */
public class StringUtils {

    public final static String CORNICE = "------------------------------------------------------------------------------";
    private final static String SPAZIO = " ";
    private final static String ACAPO = System.lineSeparator();
    private final static String[] CARATTERI = {"a", "b", "c", "d", "e", "f", "g", "h", "i",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "z", "y",
            "j", "k", "x", "w", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "Z", "Y",
            "J", "K", "X", "W", "à", "è", "é", "ì", "ò", "ù"};

    /**
     * Data una stringa in ingresso, ritorna la stessa stringa con il primo carattere maiuscolo
     * e tutti gli altri caratteri in minuscolo
     *
     * @param stringa Stringa che si vuole convertire
     * @return Stringa convertita (Primo carattere maiuscolo - tutti gli altri minuscolo)
     */
    public static String capitalizeString(String stringa) {
        return stringa.substring(0, 1).toUpperCase() + stringa.substring(1).toLowerCase();
    }

    /**
     * Verifica che una determinata stringa passata come parametro contenga solo caratteri
     *
     * @param stringa Stringa che si vuole verificare
     * @return True se la stringa contiene solo caratteri che vanno dalla 'A' alla 'Z', false altrimenti
     */
    public static boolean contieneSoloCaratteriAZ(String stringa) {
        for (int i = 0; i < stringa.length(); i++) {
            if (!((stringa.charAt(i) >= 'A' && stringa.charAt(i) <= 'Z') || (stringa.charAt(i) >= 'a' && stringa.charAt(i) <= 'z')))
                return false;
        }

        return true;
    }

    /**
     * Verifica che una determinata stringa passata come parametro contenga solo numeri
     *
     * @param stringa Stringa che si vuole verificare
     * @return True se la stringa contiene solo cifre, false altrimenti
     */
    public static boolean contieneSoloNumeri(String stringa) {
        for (int i = 0; i < stringa.length(); i++) {
            if (stringa.charAt(i) < '0' || stringa.charAt(i) > '9')
                return false;
        }

        return true;
    }

    /**
     * Ritorna una stringa racchiusa tra i caratteri di cornice
     *
     * @param s Stringa da incorniciare
     * @return La stringa compresa tra due stringhe CORNICE
     */
    public static String incornicia(String s) {
        return CORNICE + ACAPO + s + ACAPO + CORNICE + ACAPO;
    }

    /**
     * Ritorna una stringa rappresentante un elenco composto dagli elementi passati come parametro.
     * L'elenco è nel formato
     * <br>elemento1.......value1
     * <br>elemento2.......value2
     * <br>elemento_n......value_n
     * <p><i>Precondizione: la dimensione dei due array list passati come parametro deve essere la stessa</i></p>
     *
     * @param elements Elementi con vui verrà creato l'elenco
     * @param values   Valore corrispondente ad ogni elemento
     * @return Una stringa rappresentante un elenco composto dagli elementi associati ai valori passati come parametro
     */
    public static String toElenco(ArrayList<String> elements, ArrayList<String> values) {
        // Precondizione
        assert elements.size() == values.size();

        final int dimensioneMinimaCornice = 6;
        int maxLength = elements.stream()
                .map(String::length)
                .max(Integer::compareTo)
                .get();

        ArrayList<String> res = new ArrayList<>();
        for (int i = 0; i < elements.size(); i++) {
            String element = elements.get(i);
            String value = values.get(i);
            int dimensioneCornice = maxLength + 1 - element.length() + dimensioneMinimaCornice;
            res.add(element + getCornice('.', dimensioneCornice) + value);
        }

        return incornicia(res);
    }

    /**
     * Incornicia una lista di stringhe passate come parametro
     *
     * @param stringhe Stringhe che si vogliono incorniciare
     * @return Una stringa contente una cornice che racchiude la lista di stringhe passate come parametro
     */
    public static String incornicia(ArrayList<String> stringhe) {
        StringBuilder res = new StringBuilder();
        int larghezzaMax = stringhe.stream().map(String::length).max(Integer::compareTo).get() + 1;

        res.append(" ").append(getCornice('_', larghezzaMax + 2)).append(ACAPO);
        for (String s : stringhe) {
            res.append(String.format("| %s |\n", padRight(s, larghezzaMax)));
        }
        res.append(" ").append(getCornice('-', larghezzaMax + 2)).append(ACAPO);

        return res.toString();
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }

    public static String getCornice(char character, int size) {
        StringBuilder cornice = new StringBuilder();
        for (int i = 0; i < size; i++) {
            cornice.append(character);
        }
        return cornice.toString();
    }

    /**
     * Ritorna la lunghezza della stringa piu' lunga in un elenco di stringhe passato come parametro
     *
     * @param strings Elenco di stringhe su cui effettuare il controllo
     * @return La lunghezza della stringa piu' lunga dell'elenco di stringhe passato come parametro
     */
    public static int getMaxLength(ArrayList<String> strings) {
        return strings.stream()
                .map(String::length)
                .max(Integer::compareTo)
                .get();
    }
}
