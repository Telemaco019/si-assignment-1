import binarySerialization.BinarySerializationUtils;
import serialization.ClassroomProtos;
import textSerialization.Classroom;
import utils.Matematica;
import utils.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

import static utils.Constants.*;

public class Main {

    private static final boolean DEBUG = false;

    public static void main(String[] args) {
        // Text serialization
        Classroom classroom = getClassroom(NUMBER_OF_STUDENTS, DIRECTOR_NAME, CLASSROOM_NAME);
        long text_serialization_times[] = getTimesTextSerialization(classroom, NUMBER_OF_EXECUTIONS);
        long text_deserialization_times[] = getTimesTextDeserialization(Classroom.class, NUMBER_OF_EXECUTIONS, DEBUG);
        double mean_text_serialization_time = Matematica.getAverage(text_serialization_times) / Math.pow(10, 6);
        double mean_text_deserialization_time = Matematica.getAverage(text_deserialization_times) / Math.pow(10, 6);
        double standard_deviation_text_serialization = Matematica.getStandardDeviation(text_serialization_times) / Math.pow(10, 6);
        double standard_deviation_text_deserialization = Matematica.getStandardDeviation(text_deserialization_times) / Math.pow(10, 6);

        System.out.println(StringUtils.incornicia("TEXT SERIALIZATION"));
        System.out.println("*** Serialization ***");
        System.out.println("Number of executions: " + NUMBER_OF_EXECUTIONS);
        System.out.println("Number of students: " + NUMBER_OF_STUDENTS);
        System.out.println(String.format("Mean execution time: %.3f ms", mean_text_serialization_time));
        System.out.println(String.format("Standard deviation: %.3f ms", standard_deviation_text_serialization));

        System.out.println();
        System.out.println("*** Deserialization ***");
        System.out.println("Number of executions: " + NUMBER_OF_EXECUTIONS);
        System.out.println("Number of students: " + NUMBER_OF_STUDENTS);
        System.out.println(String.format("Mean execution time: %.3f ms", mean_text_deserialization_time));
        System.out.println(String.format("Standard deviation: %.3f ms", standard_deviation_text_deserialization));


        // Binary serialization
        ClassroomProtos.Classroom classroom_protobuf = BinarySerializationUtils.getClassroom(NUMBER_OF_STUDENTS, DIRECTOR_NAME, CLASSROOM_NAME);
        long binary_serialization_times[] = getTimesBinarySerialization(classroom_protobuf, NUMBER_OF_EXECUTIONS);
        long binary_deserialization_times[] = getTimesBinaryDeserialization(NUMBER_OF_EXECUTIONS, DEBUG);
        double mean_binary_serialization_time = Matematica.getAverage(binary_serialization_times) / Math.pow(10, 6);
        double mean_binary_deserialization_time = Matematica.getAverage(binary_deserialization_times) / Math.pow(10, 6);

        System.out.println("\n");
        System.out.println(StringUtils.incornicia("BINARY SERIALIZATION"));
        System.out.println("*** Serialization ***");
        System.out.println("Number of executions: " + NUMBER_OF_EXECUTIONS);
        System.out.println("Number of students: " + NUMBER_OF_STUDENTS);
        System.out.println(String.format("Mean execution time: %.3f ms", mean_binary_serialization_time));
        System.out.println(String.format("Standard deviation: %.3f ms", Matematica.getStandardDeviation(binary_serialization_times) / Math.pow(10, 6)));

        System.out.println();
        System.out.println("*** Deserialization ***");
        System.out.println("Number of executions: " + NUMBER_OF_EXECUTIONS);
        System.out.println("Number of students: " + NUMBER_OF_STUDENTS);
        System.out.println(String.format("Mean execution time: %.3f ms", mean_binary_deserialization_time));
        System.out.println(String.format("Standard deviation: %.3f ms", Matematica.getStandardDeviation(binary_deserialization_times) / Math.pow(10, 6)));
    }

    private static long[] getTimesBinaryDeserialization(int numberOfExecutions, boolean debug) {
        long[] times = new long[numberOfExecutions];

        for (int i = 0; i < numberOfExecutions; i++)
            times[i] = binaryDeserialize(debug);

        return times;
    }

    private static long binaryDeserialize(boolean debug) {
        long execution_time = 0;
        try {
            long start_time = System.nanoTime();
            FileInputStream input = new FileInputStream(BINARY_FILE_PATH);
            ClassroomProtos.Classroom classroom = ClassroomProtos.Classroom.parseFrom(input);
            input.close();

            if (debug)
                System.out.println(classroom.toString());

            long finish_time = System.nanoTime();
            execution_time = finish_time - start_time;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return execution_time;
    }

    private static long[] getTimesBinarySerialization(ClassroomProtos.Classroom classroom, int numberOfExecutions) {
        long[] times = new long[numberOfExecutions];

        for (int i = 0; i < numberOfExecutions; i++)
            times[i] = binarySerialize(classroom);

        return times;
    }

    private static long binarySerialize(ClassroomProtos.Classroom toSerialize) {
        // Time for creating and closing output stream is also considered
        long executio_time = 0;
        try {
            long start_time = System.nanoTime();

            FileOutputStream output = new FileOutputStream(BINARY_FILE_PATH);
            toSerialize.writeTo(output);
            output.close();

            long finish_time = System.nanoTime();
            executio_time = finish_time - start_time;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return executio_time;
    }

    private static long[] getTimesTextDeserialization(Class objectClass, int numberOfExecutions, Boolean debug) {
        long[] times = new long[numberOfExecutions];

        for (int i = 0; i < numberOfExecutions; i++)
            times[i] = textDeserialize(objectClass, debug);

        return times;
    }

    private static long[] getTimesTextSerialization(Classroom classroom, int numberOfExecutions) {
        long[] times = new long[numberOfExecutions];

        for (int i = 0; i < numberOfExecutions; i++)
            times[i] = textSerialize(classroom);

        return times;
    }


    /**
     * Serializes the textSerialization.Classroom passed as parameter and saves it into an xml file using JAXB
     *
     * @param classRoom textSerialization.Classroom that has to be serialized
     * @return The time required to serialize the textSerialization.Classroom, expressed in nanoseconds
     */
    private static long textSerialize(Classroom classRoom) {
        long execution_time = 0;
        try {
            File file = new File(XML_FILE_PATH);
            // Register start time of marshalling
            long start_time = System.nanoTime();

            JAXBContext context = JAXBContext.newInstance(Classroom.class);
            Marshaller marshaller = context.createMarshaller();

            // output pretty printed
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(classRoom, file);

            // Register end time of unmarshalling
            long finish_time = System.nanoTime();
            execution_time = finish_time - start_time;
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return execution_time;
    }

    /**
     * Unmarshal fromm the XML file passed as parameter, which is supposed to contain an object of the class type
     * passed as parameter
     *
     * @param objectClass Class of the object contained in the XML file to deserialize
     * @param debug       If set to true the method prints debug information
     * @return The time required to deserialize the textSerialization.Classroom contained in the XML file, expressed in nanoseconds
     */
    private static long textDeserialize(Class objectClass, Boolean debug) {
        long execution_time = 0;
        try {
            File file = new File(XML_FILE_PATH);
            // Register start time of unmarshalling
            long start_time = System.nanoTime();

            JAXBContext jaxbContext = JAXBContext.newInstance(objectClass);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Object unmarshalled_object = jaxbUnmarshaller.unmarshal(file);

            // Register end time of unmarshalling
            long finish_time = System.nanoTime();
            execution_time = finish_time - start_time;

            if (debug) {
                Classroom deserialized_classroom = (Classroom) unmarshalled_object;
                System.out.println("*** DESERIALIZED CLASSROOM ***");
                System.out.println(deserialized_classroom.toString());
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return execution_time;
    }

    private static Classroom getClassroom(int students_number, String director_name, String classroom_name) {
        Classroom classRoom = new Classroom();
        classRoom.fillWithRandomStudents(students_number);
        classRoom.createRandomDirector(director_name);
        classRoom.setName(classroom_name);

        return classRoom;
    }
}
