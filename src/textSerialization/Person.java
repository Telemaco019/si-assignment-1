package textSerialization;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

public class Person {
    private String name;
    private String email;
    private String id;
    @XmlElementWrapper(name = "telephones")
    @XmlElement(name = "telephone")
    private ArrayList<Telephone> telephones;

    private static int id_counter = 0;

    public Person(String name, String email) {
        this.name = name;
        this.email = email;
        telephones = new ArrayList<>();
        this.id = String.format("%d", id_counter);

        id_counter++;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    @XmlAttribute(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    @XmlAttribute(name = "email")
    public void setEmail(String email) {
        this.email = email;
    }

    public void addTelephone(Telephone phone) {
        telephones.add(phone);
    }

    /**
     * Add a random number of telephones. The number and the type of the telephone are random as well.
     *
     * @param number_of_telephones Number of telephones to be added.
     */
    public void addRandomTelephones(int number_of_telephones) {
        for (int i = 0; i < number_of_telephones; i++) {
            String number = String.format("%d", Math.round(Math.random() * (Math.pow(10, 10))));
            Telephone phone = new Telephone(number);
            addTelephone(phone);
        }
    }

    public String toString() {
        return String.format("Id: %s \t Name: %s \t Email: %s \t Telephones: %s",
                this.getId(),
                this.getName(),
                this.getEmail(),
                telephones.toString());
    }
}
