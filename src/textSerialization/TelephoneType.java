package textSerialization;

enum TelephoneType {

    HOME("Home"), WORK("Work"), MOBILE("Mobile");
    private String name;

    TelephoneType(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}