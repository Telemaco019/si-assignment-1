package textSerialization;

import utils.Matematica;

public class Telephone {
    String number;
    TelephoneType type;

    /**
     * Create a new telephone. The number is the one passed as parameter, while the type is randomly chosen from
     * the enum {@link TelephoneType}
     * @param number
     */
    public Telephone(String number) {
        this.number = number;
        TelephoneType types[] = TelephoneType.values();
        this.type = types[Matematica.numeroInteroRandom(types.length)];
    }

    public Telephone() {

    }

    public TelephoneType getType() {
        return type;
    }

    public void setType(TelephoneType type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String toString() {
        return String.format("Number: %s - Type: %s", number, type);
    }
}
