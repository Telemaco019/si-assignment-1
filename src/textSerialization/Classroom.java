package textSerialization;

import utils.Constants;
import utils.Matematica;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import static utils.Constants.EMAIL_DOMAIN;

@XmlRootElement
public class Classroom {

    private String name;
    private Director director;
    @XmlElementWrapper(name = "student_list")
    @XmlElement(name = "student")
    private ArrayList<Person> students;

    public Classroom() {
        students = new ArrayList<>();
    }

    @XmlAttribute(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Person> getStudents() {
        return students;
    }

    public void addStudent(Student toAdd) {
        students.add(toAdd);
    }

    /**
     * Add students to the classroom. Name of the student and its email are sequential, e.g. "student 1", "student 2", etc.
     * Telephones are added randomly: for each student are added a random number of telephones included in 1 and
     * num_max_telephones.
     *
     * @param students_number Number of students to be added.
     */
    public void fillWithRandomStudents(int students_number) {
        for (int i = 0; i < students_number; i++) {
            String student_name = "student-" + i;
            Student s = new Student(student_name, student_name + EMAIL_DOMAIN);
            int num_telephones = Matematica.numeroInteroRandom(Constants.NUM_MAX_TELEPHONES_PER_PERSON) + 1;
            s.addRandomTelephones(num_telephones);
            addStudent(s);
        }
    }

    /**
     * Create a random director using the name passed as parameter and set it as the director of the classroom.
     * The mail address will be "director_name@EMAIL_DOMAIN".
     * Telephones are added randomly: a random number of telephones included in 1 and num_max_telephones are added.
     */
    public void createRandomDirector(String name) {
        Director director = new Director(name, name + EMAIL_DOMAIN);
        int num_max_telephones = 3;
        int num_telephones = Matematica.numeroInteroRandom(num_max_telephones) + 1;
        director.addRandomTelephones(num_telephones);
        setDirector(director);
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("*** Students: ****\n");
        for (Person s : getStudents())
            result.append(s.toString() + "\n");
        result.append("*** textSerialization.Director: ***\n");
        result.append(getDirector().toString() + "\n");

        return result.toString();
    }

    public Director getDirector() {
        return director;
    }
}
